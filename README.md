# README

Dieses Repository brauchen wir f�r die Git Schulung :-)

## Fragestellung

Was passiert, wenn alle gleichzeitig auf dem "master" branch arbeiten?

 * bei �nderungen an unterschiedlichen Dateien
 * bei �nderungen in der gleichen Datei (aber in unterschiedlichen Zeilen)
 * bei �nderungen in der gleichen Zeile

## Praktische �bung

Teil 1:

 * spielen Sie verschiedene Varianten durch und schauen Sie was passiert.
 * versuchen sie Konflikte via `git mergetool` zu l�sen
 
Teil 2:

 * nutzen sie einen Pull Request, um ihre �nderung einzubringen